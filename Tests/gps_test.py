#Tests GNSS module printing info received until Interrupted by keyboard
import serial #pip3 install serial
import time
import sys

ser = serial.Serial("/dev/ttyS0",115200)

W_buff = ["AT+CGNSPWR=1\r\n", "AT+CGNSSEQ=\"RMC\"\r\n", "AT+CGNSINF\r\n", "AT+CGNSURC=2\r\n", "AT+CGNSTST=1\r\n"]
ser.write(W_buff[0])
ser.flushInput()
data = ""
num = 0

try:
    while True:
        while ser.inWaiting() > 0:
            data += ser.read(ser.inWaiting())
        if data != "":
            print(data)
            time.sleep(0.5)
            ser.write(W_buff[num+1])
            num += 1
            if num == 4:
                time.sleep(0.5)
                ser.write(W_buff[4])
            data = ""
except KeyboardInterrupt:
    if ser != None:
        try:
            ser.close()
        except Exception as e:
            print("Exception trying to close serial connection: {}".format(e))
    try:
        sys.exit(0)
    except SystemExit:
        os._exit(0)
